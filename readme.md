# The Best Mens
---
The Best Mens is a website for users to products are most popular, best rated and animal friendly (using data from PETA).

## Features
---
- Users can mark products as 'using', 'want to use' or 'previously used'. This is used for tracking how popular a product is (popularity on other sites is measured by number of reviews or sales).
- Users can rate and review products.
    - Reviewers add personal information to their profiles to allow for more detailed metrics compared to other review sites. For example '75% of users with Dry skin, who use face moisturiser, are using this product.'  
- Public profiles, for sharing lists of products with others.
- Product names, brands and images scraped from Amazon.
- Amazon affiliate links for revenue generation.
- Integrated data from PETA, to check if a product is made by a vegan or cruelty free brandLinks to   

## Built with
---
- Django

---
![Product list](product_list.png)
![Product](product.png)
![Profile](profile.png)
