from django.contrib.auth import login, get_user_model
from django.contrib.auth.views import LoginView
from django.db.models import Avg, Count, OuterRef, Subquery
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from django.core.paginator import Paginator

from .filters import ProductFilter
from .forms import (ReviewForm, SkinTypeForm, CustomAuthenticationForm, CustomUserCreationForm, ProfileChangeForm,
                    CustomUserChangeForm, ProfileCreationForm, UserActivityForm)
from .models import Product, Profile, Review, Category, Country, Brand, RetailerProduct, UserActivity

STAR_LIST = [[1, 0.5], [2, 1.5], [3, 2.5], [4, 3.5], [5, 4.5]]


class CustomLoginView(LoginView):
    authentication_form = CustomAuthenticationForm
    redirect_authenticated_user = True
    template_name = 'app/login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['country_code'] = self.kwargs['country_code']
        return context


def signup(request, country_code):
    if request.method == 'POST':
        user_form = CustomUserCreationForm(request.POST)
        profile_form = ProfileCreationForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            user.username = user.email.split('@')[0]  # Set the username to first part of the email as default
            # TODO need to check for existing matching usernames.
            user.save()
            user.refresh_from_db()

            profile_form = ProfileCreationForm(request.POST, instance=user.profile)
            profile_form.full_clean()
            profile_form.save()

            login(request, user)
            # could send user back to the previous page instead of the home page on signup?
            return redirect('index', country_code=country_code)
    else:
        user_form = CustomUserCreationForm()
        country = get_object_or_404(Country, code=country_code.upper())
        profile_form = ProfileCreationForm(initial={'country': country,
                                                    'gender': Profile.MALE})

    context = {'user_form': user_form,
               'profile_form': profile_form,
               'country_code': country_code}
    return render(request, 'app/signup.html', context=context)


def profile(request, country_code, username):
    user = get_object_or_404(get_user_model(), username=username)

    products_currently_using = (Product.objects
                                .filter(**{'useractivity__user': user,
                                           'useractivity__status': UserActivity.USING})
                                .annotate())
    reviews = Review.objects.filter(user=user).all().order_by('-last_updated')

    context = {'products_currently_using': products_currently_using,
               'reviews': reviews,
               'country_code': country_code,
               'username': username,
               'star_list': STAR_LIST}
    return render(request, 'app/profile.html', context=context)


def edit_profile(request, country_code):
    form_message = None

    if request.method == 'POST':
        user_form = CustomUserChangeForm(request.POST, instance=request.user)
        profile_form = ProfileChangeForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            form_message = 'Profile updated.'
    else:
        user_form = CustomUserChangeForm(instance=request.user)
        profile_form = ProfileChangeForm(instance=request.user.profile)

    context = {'user_form': user_form,
               'profile_form': profile_form,
               'country_code': country_code,
               'form_message': form_message}
    return render(request, 'app/edit_profile.html', context=context)


def index(request, country_code):
    context = {
        'country_code': country_code,
    }
    return render(request, 'app/index.html', context=context)


def get_products(product_filter):
    review_sub_objects = Review.objects.filter(product=OuterRef('id')).values('product')
    review_sub_query = review_sub_objects.annotate(review__count=Count('id'), review__rating__avg=Avg('rating'))

    retailer_product_sub_objects = RetailerProduct.objects.filter(product=OuterRef('id')).order_by('price__price')

    product_annotations = {
        'review__count': Subquery(review_sub_query.values('review__count')),
        'review__rating__avg': Subquery(review_sub_query.values('review__rating__avg')),
        # retailerproduct__price__price__min=Subquery(retailer_product_sub_objects.values('price__price')[:1]),
        # retailerproduct__price__price_per_100_ml__min=Subquery(retailer_product_sub_objects.values('price__price_per_100_ml')[:1]),
        'retailerproduct__retailer__name': Subquery(retailer_product_sub_objects.values('retailer__name')[:1]),
        # retailerproduct__price__last_updated=Subquery(retailer_product_sub_objects.values('price__last_updated')[:1]),
        # retailerproduct__retailer__currency__symbol=Subquery(retailer_product_sub_objects.values('retailer__currency__symbol')[:1])

    }

    products = (Product.objects
                .filter(**product_filter)
                .annotate(**product_annotations)
                .distinct())
    return products


def top(request, country_code, category_slug, category_filter):
    category = get_object_or_404(Category, name_slug=category_slug)

    product_filter = {'retailerproduct__retailer__country__code': country_code.upper(),
                      'category': category,
                      'review__user__profile__gender': Profile.MALE}

    if category_filter != 'all':
        if category.filter_type == 'skin':
            if category_filter not in Profile.SKIN_TYPE_CODES:
                raise Http404('Unrecognised skin type.')
            product_filter['review__user__profile__skin_type'] = category_filter

    products = get_products(product_filter).order_by('-review__rating__avg')

    best_over_20 = products.filter(retailerproduct__price__price__min__gt=20.0)
    best_between_10_and_20 = products.filter(retailerproduct__price__price__min__range=(10.01, 20.0))
    best_between_5_and_10 = products.filter(retailerproduct__price__price__min__range=(5.01, 10.0))
    best_5_and_under = products.filter(retailerproduct__price__price__min__lte=5.0)

    best_over_20 = best_over_20[0:2] if best_over_20.exists() else None
    best_between_10_and_20 = best_between_10_and_20[0:2] if best_between_10_and_20.exists() else None
    best_between_5_and_10 = best_between_5_and_10[0:2] if best_between_5_and_10.exists() else None
    best_5_and_under = best_5_and_under[0:2] if best_5_and_under.exists() else None

    # import ipdb;ipdb.set_trace()
    # price_categories = [best_over_20, best_between_10_and_20, best_between_5_and_10, best_5_and_under]
    # for i, price_category in enumerate(price_categories):
    #     if price_category.exists():
    #         price_categories[i] = price_category[0]  # Take the first object
    #     else:
    #         price_categories[i] = None

    context = {
        'country_code': country_code,
        'currency_symbol': '£',
        'category': category,
        'category_filter': category_filter,
        'skin_type_choices': dict(Profile.SKIN_TYPE_CHOICES),
        'gender': 'Men',
        'best_over_20': best_over_20,
        'best_between_10_and_20': best_between_10_and_20,
        'best_between_5_and_10': best_between_5_and_10,
        'best_5_and_under': best_5_and_under,
        'star_list': STAR_LIST
    }

    return render(request, 'app/top.html', context)


def products(request, country_code):
    product_filter = {}
    product_queryset = get_products(product_filter)
    filter = ProductFilter(request.GET, queryset=product_queryset)

    paginator = Paginator(filter.qs, 10)
    page_number = request.GET.get('page', 1)
    products = paginator.get_page(page_number)

    context = {
        'country_code': country_code,
        'products': products,
        'star_list': STAR_LIST,
        'filter': filter,
    }
    return render(request, 'app/products.html', context=context)


def product(request, country_code, product_slug):
    get_object_or_404(Product, name_slug=product_slug)
    review_form = skin_type_form = authentication_form = review_form_message = user_activity_form = user_activity_form_message = None

    product_filter = {'retailerproduct__retailer__country__code': country_code.upper(),
                      'name_slug': product_slug}
    product = get_products(product_filter).first()  # Why do we also call get_object_or_404 above?

    users_using_product_in_category_count = get_user_model().objects.filter(
        **{'useractivity__product__category': product.category,
           'useractivity__status': UserActivity.USING}
    ).distinct().count()

    if product.users_using_count and users_using_product_in_category_count:
        users_using_percent = int((product.users_using_count / users_using_product_in_category_count) * 100.0)
    else:
        users_using_percent = 0

    users_using_rank = Product.objects.filter(category=product.category,
                                              users_using_count__gt=product.users_using_count).count() + 1

    category_product_count = Product.objects.filter(category=product.category).count()

    if request.user.is_authenticated:
        review = Review.objects.filter(product=product, user=request.user).first()
        user_activity = UserActivity.objects.filter(product=product, user=request.user).first()
        if request.method == 'POST':
            # Don't know how to explicitly check for user activity form submission, so check for review form
            if 'review_submit' in request.POST:
                skin_type_form = SkinTypeForm(request.POST, instance=request.user.profile)
                review_form = ReviewForm(request.POST, instance=review)
                if review_form.is_valid() and skin_type_form.is_valid():
                    valid_review_form = review_form.save(commit=False)
                    valid_review_form.product = product
                    valid_review_form.user = request.user
                    valid_review_form.save()

                    valid_skin_type_form = skin_type_form.save(commit=False)
                    valid_skin_type_form.user = request.user
                    valid_skin_type_form.save()

                    review_form_message = 'Thanks for your review!'

                user_activity_form = UserActivityForm(instance=user_activity)
            else:
                user_activity_form = UserActivityForm(request.POST, instance=user_activity)
                if user_activity_form.is_valid():
                    valid_user_activity_form = user_activity_form.save(commit=False)
                    valid_user_activity_form.product = product
                    valid_user_activity_form.user = request.user
                    valid_user_activity_form.save()

                    user_activity_form_message = 'Status updated!'
                skin_type_form = SkinTypeForm(instance=request.user.profile)
                review_form = ReviewForm(instance=review)
        else:
            skin_type_form = SkinTypeForm(instance=request.user.profile)
            review_form = ReviewForm(instance=review)
            user_activity_form = UserActivityForm(instance=user_activity)
    else:
        authentication_form = CustomAuthenticationForm()

    context = {'country_code': country_code,
               'product': product,
               'star_list': STAR_LIST,
               'review_form_message': review_form_message,
               'review_form': review_form,
               'skin_type_form': skin_type_form,
               'authentication_form': authentication_form,
               'user_activity_form_message': user_activity_form_message,
               'user_activity_form': user_activity_form,
               'users_using_percent': users_using_percent,
               'users_using_rank': users_using_rank,
               'category_product_count': category_product_count}

    return render(request, 'app/product.html', context=context)


class Categories(ListView):
    model = Category
    context_object_name = 'categories'
    template_name = 'app/categories.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['country_code'] = self.kwargs['country_code']
        context['skin_type_choices'] = dict(Profile.SKIN_TYPE_CHOICES)
        return context


class Brands(ListView):
    model = Brand
    context_object_name = 'brands'
    template_name = 'app/brands.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['country_code'] = self.kwargs['country_code']
        return context
