from django.urls import path, re_path, include

from . import views

urlpatterns = [
    re_path(r'^(?P<country_code>[a-z]{2})/$', views.index, name='index'),
    re_path(r'^(?P<country_code>[a-z]{2})/product/(?P<product_slug>[-a-zA-Z0-9_]+)$', views.product, name='product'),
    re_path(r'^(?P<country_code>[a-z]{2})/products$', views.products, name='products'),
    re_path(r'^(?P<country_code>[a-z]{2})/brands/$', views.Brands.as_view(), name='brands'),
    re_path(r'^(?P<country_code>[a-z]{2})/categories/$', views.Categories.as_view(), name='categories'),
    re_path(r'^(?P<country_code>[a-z]{2})/top/(?P<category_slug>[-a-zA-Z0-9_]+)/(?P<category_filter>[-a-zA-Z0-9_]+)/$', views.top, name='top'),
    re_path(r'^(?P<country_code>[a-z]{2})/login/$', views.CustomLoginView.as_view(), name='login'),
    re_path(r'^(?P<country_code>[a-z]{2})/signup/$', views.signup, name='signup'),
    re_path(r'^(?P<country_code>[a-z]{2})/profile/(?P<username>[-a-zA-Z0-9_]+)$', views.profile, name='profile'),
    re_path(r'^(?P<country_code>[a-z]{2})/edit-profile/$', views.edit_profile, name='edit_profile'),
    path('accounts/', include('django.contrib.auth.urls')),
]
