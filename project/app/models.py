from datetime import datetime

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Count
from django.utils.text import slugify
from stdimage import StdImageField


# TODO add model for data source


class Country(models.Model):
    class Meta:
        verbose_name_plural = 'countries'

    code = models.CharField(max_length=2, unique=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Currency(models.Model):
    class Meta:
        verbose_name_plural = 'currencies'

    iso_code = models.CharField(max_length=3, unique=True)
    symbol = models.CharField(max_length=1)

    def __str__(self):
        return self.iso_code


class CustomUser(AbstractUser):
    # TODO remove last name field?
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def save(self, *args, **kwargs):
        is_new_user = self._state.adding
        super().save(*args, **kwargs)
        # Create Profile on creation of CustomUser.
        # Overriding save() instead of using signals: https://lincolnloop.com/blog/django-anti-patterns-signals/
        if is_new_user:
            profile = Profile(user=self)
            profile.save()


class Profile(models.Model):
    NOT_SPECIFIED = 'N'
    blank_choice = [(None, '---------')]
    BIRTH_YEAR_CHOICES = [(r, str(r)) for r in reversed(range(datetime.today().year - 100, datetime.today().year - 13))]  # No kids allowed

    MALE = 'M'
    FEMALE = 'F'
    GENDER_CHOICES = [(MALE, 'Male'),
                      (FEMALE, 'Female'),
                      (NOT_SPECIFIED, 'Prefer not to say')]
    # skin_type
    DRY = 'dry'
    NORMAL = 'normal'
    OILY = 'oily'
    COMBINATION = 'combination'
    SENSITIVE = 'sensitive'
    SKIN_TYPE_CODES = [DRY, NORMAL, OILY, COMBINATION, SENSITIVE, NOT_SPECIFIED]
    SKIN_TYPE_CHOICES = [(DRY, 'Dry'),
                         (NORMAL, 'Normal'),
                         (OILY, 'Oily'),
                         (COMBINATION, 'Combination'),
                         (SENSITIVE, 'Sensitive'),
                         (NOT_SPECIFIED, 'Prefer not to say')]

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    # Allowing all of these fields to be blank so we can create a record in Profile when saving the CustomUser model:
    country = models.ForeignKey(Country, on_delete=models.PROTECT, blank=True, null=True)
    birth_year = models.PositiveSmallIntegerField(choices=BIRTH_YEAR_CHOICES, blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True, null=True)
    skin_type = models.CharField(max_length=11, choices=SKIN_TYPE_CHOICES, blank=True)


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'categories'
        ordering = ["-name"]

    name = models.CharField(max_length=50, unique=True)
    name_slug = models.SlugField(max_length=50)

    filter_type = models.CharField(max_length=4, choices=[('skin', 'Skin Type')], blank=True)

    def save(self, *args, **kwargs):
        self.name_slug = slugify(self.name)   # Note this will break URLs
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class BrandParent(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Peta(models.Model):
    valid_from = models.DateField()
    valid_to = models.DateField(blank=True, null=True)
    company_name = models.CharField(max_length=100)
    peta_logo = models.BooleanField(null=True, blank=True)
    vegan_notes = models.CharField(max_length=300, blank=True)
    vegan = models.BooleanField(null=True, blank=True)
    testing_policy = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return ' | '.join([self.company_name, str(self.testing_policy)])


class Brand(models.Model):
    name = models.CharField(max_length=50, unique=True)
    name_slug = models.SlugField(max_length=50)

    parent = models.ForeignKey(BrandParent, on_delete=models.PROTECT, null=True, blank=True)
    peta = models.ForeignKey(Peta, on_delete=models.SET_NULL, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.name_slug = slugify(self.name)   # Note this will break URLs
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Product(models.Model):
    # Note product sharing same name:
    # Face & Stubble Care Kit by Percy Nobleman, A Men's Gift Set For Skin Care. Moisturiser, Wash and Face Towel Kit
    name = models.CharField(max_length=200)
    name_slug = models.SlugField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)

    brand = models.ForeignKey(Brand, on_delete=models.PROTECT, blank=True, null=True)
    ingredients = models.CharField(max_length=1000, blank=True)
    description = models.CharField(max_length=1000, blank=True)
    directions = models.CharField(max_length=1000, blank=True)
    volume_ml = models.PositiveSmallIntegerField(blank=True, null=True)  # TODO What if a product can have different sizes?
    made_in = models.ForeignKey(Country, on_delete=models.PROTECT, blank=True, null=True)  # Assumes product is only made in one country
    std_image = StdImageField(
        upload_to='product_images/',
        variations={'small': {"width": 125, "height": 250}},
        delete_orphans=True,
        blank=True
    )

    users_using_count = models.PositiveSmallIntegerField(default=0)

    def users_using_rank(self):
        rank = Product.objects.filter(users_using_count__gt=self.users_using_count).count() + 1
        return rank

    def save(self, *args, **kwargs):
        self.name_slug = slugify(self.name)   # Note this will break URLs
        super().save(*args, **kwargs)

    def __str__(self):
        if self.brand:
            return ' | '.join([self.brand.name, self.name])
        else:
            return self.name


class Retailer(models.Model):
    name = models.CharField(max_length=50, unique=True)
    country = models.ForeignKey(Country, on_delete=models.PROTECT)
    currency = models.ForeignKey(Currency, on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class RetailerProduct(models.Model):
    product = models.ForeignKey(Product, blank=True, null=True, on_delete=models.SET_NULL)
    retailer = models.ForeignKey(Retailer, on_delete=models.PROTECT)
    category = models.CharField(max_length=100)
    name = models.CharField(max_length=500)
    url = models.URLField()
    is_for_women = models.BooleanField()
    is_multipack = models.BooleanField()

    brand = models.CharField(max_length=200, blank=True)
    manufacturer = models.CharField(max_length=200, blank=True)
    ingredients = models.CharField(max_length=1000, blank=True)
    description = models.CharField(max_length=1000, blank=True)
    directions = models.CharField(max_length=1000, blank=True)
    volume_ml = models.PositiveSmallIntegerField(blank=True, null=True)
    gender = models.CharField(max_length=100, blank=True)
    country_of_origin = models.CharField(max_length=100, blank=True)
    identifier = models.CharField(max_length=10, blank=True)
    rating_average = models.PositiveSmallIntegerField(blank=True, null=True)
    rating_count = models.PositiveSmallIntegerField(blank=True, null=True)
    last_updated = models.DateTimeField(auto_now=True)
    # updated_by = models.CharField(max_length=10, blank=True)
    # when_entered = models.DateTimeField(auto_now=True)
    # entered_by = models.CharField(max_length=10)
    std_image = StdImageField(
        upload_to='retailer_product_images/',
        variations={'small': {"width": 125, "height": 250}},
        delete_orphans=True,
        blank=True
    )

    def __str__(self):
        return ' | '.join([self.retailer.name, self.name])

    # TODO If volume_ml is updated, we should update price_per_ml in Price


class Price(models.Model):
    retailer_product = models.OneToOneField(RetailerProduct, on_delete=models.CASCADE)
    price = models.FloatField(validators=[MinValueValidator(0.0)])
    last_updated = models.DateTimeField(auto_now=True)
    # updated_by = models.CharField(max_length=10)
    price_per_100_ml = models.FloatField(validators=[MinValueValidator(0.0)], blank=True, null=True)
    # TODO store price history?

    def save(self, *args, **kwargs):
        if self.retailer_product.volume_ml:
            self.price_per_100_ml = round(self.price / self.retailer_product.volume_ml * 100.0, 2)
        super().save(*args, **kwargs)

    def __str__(self):
        return ' | '.join([self.retailer_product.retailer.name, self.retailer_product.brand, self.retailer_product.name, str(self.price), self.retailer_product.retailer.currency.iso_code])


class Review(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['product', 'user'], name='unique_review')
        ]

    RATING_CHOICES = [
        (1, "Don't like it"),
        (2, "It's OK"),
        (3, 'Like it'),
        (4, 'Really like it'),
        (5, "It's amazing")
    ]

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    rating = models.IntegerField(choices=RATING_CHOICES)
    review = models.CharField(max_length=1000, blank=True)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return ' | '.join([self.product.name, self.user.email])


class UserActivity(models.Model):
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['product', 'user'], name='unique_activity')
        ]
        verbose_name_plural = 'user activity'

    BLANK = ''
    WANT = 'want'
    USING = 'using'
    USED = 'used'
    ACTIVITY_CODES = [WANT, USING, USED]
    ACTIVITY_CHOICES = [(BLANK, '---------'), (WANT, 'I want to use this'), (USING, "I'm using this"), (USED, "I've used this")]

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    status = models.CharField(max_length=5, choices=ACTIVITY_CHOICES, blank=True)
    # Note when 'removing' a status we just set it to '' instead of deleting

    # TODO add tracking of statuses over time.
    # TODO reduce users_using_count if user is deleted.
    def save(self, *args, **kwargs):
        # Increment or decrement users_using_count
        try:
            original_status = UserActivity.objects.get(user=self.user, product=self.product).status
        except UserActivity.DoesNotExist:
            original_status = self.BLANK

        super().save(*args, **kwargs)
        if original_status == self.USING and self.status != self.USING:
            Product.objects.filter(id=self.product.id).update(users_using_count=self.product.users_using_count-1)
        elif original_status != self.USING and self.status == self.USING:
             Product.objects.filter(id=self.product.id).update(users_using_count=self.product.users_using_count+1)
