import csv
import json
import logging
import re

from app.models import Retailer, Category, RetailerProduct, Product, Price, Brand
from django.core.files import File
from django.core.management import BaseCommand
from django.db import transaction
from django.db.models import Count
from django.shortcuts import get_object_or_404

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Command(BaseCommand):
    help = 'Loads scraped Amazon product data.'

    @staticmethod
    def open_amazon_json_from_file(filename):
        with open(filename, 'r') as f:
            amazon_json = f.read()
        return json.loads(amazon_json)

    @transaction.atomic
    def parse_and_save(self, amazon_data, category, retailer, image_directory_path):
        existing_asins = RetailerProduct.objects.values_list('identifier', flat=True).all()
        for amazon_product in amazon_data:
            if amazon_product['asin'] in existing_asins:
                logger.info(f'Product already loaded. Skipping: {amazon_product["asin"]} {amazon_product["title"]}')
                continue

            # Use empty string instead of None to keep Django happy
            optional_string_fields = ['brand', 'manufacturer', 'pet_type', 'country_of_origin', 'asin']
            for field in optional_string_fields:
                if amazon_product[field] is None:
                    amazon_product[field] = ''

            is_for_women, is_multipack = self.detect_women_and_multipack_product(amazon_product)

            image_path = image_directory_path + amazon_product['images'][0]['path']
            image_filename = amazon_product['images'][0]['path'].lstrip('full/')

            product = self.create_product(amazon_product, category, image_filename, image_path, is_for_women,
                                          is_multipack)
            retailer_product = self.create_retailer_product(amazon_product, category, image_filename, image_path,
                                                            product, retailer)
            if amazon_product['price']:
                self.create_price(amazon_product, retailer_product)

    @staticmethod
    def detect_women_and_multipack_product(amazon_product):
        is_for_women = is_multipack = False
        if (re.match(r'women', amazon_product['pet_type'], re.IGNORECASE)
                or re.search(r'for women(?! (?:and|&) men)', amazon_product['title'], re.IGNORECASE)):
            logger.info(f'Detected product for women: {amazon_product["asin"]} {amazon_product["title"]}')
            is_for_women = True
        if re.search(r' pack| set|\dx\d', amazon_product['title'], re.IGNORECASE):
            logger.info(f'Detected multipack product: {amazon_product["asin"]} {amazon_product["title"]}')
            is_multipack = True
        return is_for_women, is_multipack

    @staticmethod
    def create_price(amazon_product, retailer_product):
        price = Price(retailer_product=retailer_product,
                      price=amazon_product['price'])
        price.save()

    @staticmethod
    def create_retailer_product(amazon_product, category, image_filename, image_path, product, retailer):
        retailer_product = RetailerProduct(product=product,
                                           retailer=retailer,
                                           category=category.name,
                                           brand=amazon_product['brand'],
                                           manufacturer=amazon_product['manufacturer'],
                                           gender=amazon_product['pet_type'],
                                           country_of_origin=amazon_product['country_of_origin'],
                                           name=amazon_product['title'],
                                           url=amazon_product['url'],
                                           identifier=amazon_product['asin'],
                                           rating_average=amazon_product['rating_average'],
                                           rating_count=amazon_product['rating_count'])
        with open(image_path, 'rb') as image:
            image_file = File(image)
            retailer_product.std_image.save(image_filename, image_file)
            retailer_product.save()
        return retailer_product

    @staticmethod
    def create_product(amazon_product, category, image_filename, image_path, is_for_women, is_multipack):
        if not is_for_women and not is_multipack:
            product = Product(name=amazon_product['title'],
                              category=category)
            with open(image_path, 'rb') as image:
                image_file = File(image)
                product.std_image.save(image_filename, image_file)
                product.save()
        else:
            product = None
        return product

    def handle(self, *args, **options):
        retailer_brand_counts = RetailerProduct.objects.values('brand').annotate(count=Count('brand')).order_by('-count')
        common_retailer_brands = retailer_brand_counts.filter(count__gt=1).values_list('brand', flat=True)
        # TODO finish me!

        for retailer_brand in common_retailer_brands:
            obj, created = Brand.objects.get_or_create(name=retailer_brand)
