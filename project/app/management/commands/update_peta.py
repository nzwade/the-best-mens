import json

import requests
from django.core.management import BaseCommand
from django.db import transaction
from django.utils import timezone

from app.models import Peta


class Command(BaseCommand):
    help = 'Gets cruelty free data from the PETA website and saves it to the Peta model.'

    @staticmethod
    def get_json_from_url(url, payload):
        r = requests.get(url, params=payload)
        r.raise_for_status()
        # print(r.headers)  # 'X-WP-TotalPages' is not returning anything so we don't know how many pages :(
        print(r.url)
        return r.json()

    def get_peta_json_from_api(self):
        peta_json = True
        peta_json_all = []
        url = 'https://crueltyfree.peta.org/wp-json/peta_cfcs_company_endpoint/v2/company'
        payload = {'per_page': 100, 'page': 1}
        while peta_json:  # Loop through each page until calling the API returns an empty list
            peta_json = self.get_json_from_url(url, payload)
            peta_json_all.extend(peta_json)
            payload['page'] += 1
        return peta_json_all

    @staticmethod
    def save_peta_json_to_file(peta_json):
        date_now = timezone.now().strftime("%Y%m%d")
        filename = f'peta_{date_now}.json'
        with open(filename, 'w') as f:
            json.dump(peta_json, f)

    @staticmethod
    def open_peta_json_from_file(filename):
        with open(filename, 'r') as f:
            peta_json = f.read()
        return json.loads(peta_json)

    @staticmethod
    def parse_peta_json(peta_json):
        parsed_peta_json = []
        for company in peta_json:
            parsed_company = Peta(company_name=company['company_name'],
                                  peta_logo=company.get('peta_logo', False),
                                  vegan=company.get('vegan', False),
                                  vegan_notes=company.get('vegan_notes', ''),
                                  testing_policy=company.get('testing_policy', False),
                                  valid_from=timezone.now())
            parsed_peta_json.append(parsed_company)

        return parsed_peta_json

    @staticmethod
    @transaction.atomic
    def save_parsed_peta_data(parsed_peta_json):
        # FIXME this is not good as it will break Peta>Brand mappings.
        Peta.objects.update(valid_to=timezone.now())  # Invalidate all previous peta data
        for peta in parsed_peta_json:
            peta.save()

    def add_arguments(self, parser):
        parser.add_argument('--save_to_file', action='store_true', help='Save the JSON to a file.')
        parser.add_argument('--open_from_file', action='store_true', help='Use a JSON file instead of PETA API.')
        parser.add_argument('--filename', help='Filename if opening a JSON file.')

    def handle(self, *args, **options):
        if options['open_from_file']:
            peta_json = self.open_peta_json_from_file(options['filename'])
        else:
            peta_json = self.get_peta_json_from_api()

        if options['save_to_file']:
            self.save_peta_json_to_file(peta_json)

        parsed_peta_data = self.parse_peta_json(peta_json)
        self.save_parsed_peta_data(parsed_peta_data)
        self.stdout.write(self.style.SUCCESS('Successfully updated Peta model.'))
