import csv
import json
import logging
import re

from app.models import Retailer, Category, RetailerProduct, Product, Price, Brand
from django.core.files import File
from django.core.management import BaseCommand
from django.db import transaction
from django.shortcuts import get_object_or_404
import pandas as pd


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Command(BaseCommand):
    help = 'Loads scraped Amazon product data.'

    MANUFACTURER_CLEANUP_DICT = {
        "Neal's Yard (Natural) Remedies LTD": "Neal’s Yard Remedies",
        'Anthony Brands': 'Anthony',
        'Elemis': 'ELEMIS',
        'MOLTON BROWN': 'Molton Brown',
        'E.T. Browne Drug Company, Inc. (Palmers)': "Palmer's"
    }

    BRAND_CLEANUP_DICT = {
        "Brickell Men's Products": 'Brickell',
        'Bulldog Skincare': 'Bulldog',
        'DR ORGANIC': 'dr.organic',
        'E.l.f.': 'e.l.f.',
        'Eclat Skincare': 'Eclat',
        'Kiehl´s': "Kiehl's",
        'L´Occitane': "L'Occitane",
        'Mend Skincare': 'mend',
        'Nivea': 'NIVEA',
        'Nivea Men': 'NIVEA',
        'NIVP8': 'NIVEA',
        'NIVEA Men': 'NIVEA',
        'Naturally Tribal Skincare': 'Naturally Tribal',
        'No7 Men': 'No7',
        'Recipe for Men raw Naturals': 'Raw Naturals',
        'SkinActive Face': 'Garnier',
        'THE REAL SHAVING COMPANY': 'The Real Shaving Co.',
        'TRADEMARK GNARLY JOE EST.MMXVII': 'Gnarly Joe',
        'Tea tree oil australian bodycare': 'Australian Bodycare',
        'Men Expert Skin Care': "L'Oréal",
        'Ecodrop essential oils': 'ecodrop',
        'SALIN DE BIOSEL': 'SALIN de BIOSEL',
        'Anti-wrinkle Moisturiser For Men': 'No7',
        'Men Expert': "L'Oréal",
        'UPCIRCLE': 'UpCircle',
        'No.7': 'No7',
        'KENNEDY & CO': 'Kennedy & Co',
        'The Natural man': 'The Natural Man',
        'Nivea for Men': 'NIVEA',
        'FOLS For Men': 'FOLS',
        'Anti-Ageing': 'No7',
        'Dove Cream': 'Dove',
        'PureC60OliveOil.com': 'C60',
        'ARAMIS LAB SERIES': 'Lab Series',
        'St Moriz': 'St. Moriz',
        "Palmer's Cocoa Butter": "Palmer's",
        'Palmers': "Palmer's",
        'NIVEA Visage': 'NIVEA',
        "L'Oreal": "L'Oréal",
        'No7 Protect & Perfect Intense Advanced Day Cream': 'No7',
        'Men´s Master': "Men's Master",
        'FREEDERM': 'Freederm',
        "L'Oréal Men Expert": "L'Oréal",
        'Grace & Stella Co.': 'Grace & Stella',
        'Vitamin C Glow Boosting Moisturiser': 'The Body Shop',
        'TABAIBA': 'Tabaibaloe',
        'Colibri cosmetics': 'colibri',
        'ST TROPEZ': 'St. Tropez',
        'ASTRAL': 'Astral',
        'HAWAIIAN Tropic': 'Hawaiian Tropic',
        'THE ORDINARY': 'The Ordinary',
        'White Hinge': 'Rockface'
    }

    @staticmethod
    def open_amazon_json_from_file(filename):
        with open(filename, 'r') as f:
            amazon_json = f.read()
        return json.loads(amazon_json)

    @transaction.atomic
    def parse_and_save(self, amazon_data, category, retailer, image_directory_path):
        existing_asins = RetailerProduct.objects.values_list('identifier', flat=True).all()
        for amazon_product in amazon_data:
            if amazon_product['asin'] in existing_asins:
                logger.info(f'Product already loaded. Skipping: {amazon_product["asin"]} {amazon_product["title"]}')
                continue

            # Use empty string instead of None to keep Django happy
            optional_string_fields = ['brand', 'manufacturer', 'pet_type', 'country_of_origin', 'asin']
            for field in optional_string_fields:
                if amazon_product[field] is None:
                    amazon_product[field] = ''

            amazon_product = self.detect_women_and_multipack_product(amazon_product)

            image_path = image_directory_path + amazon_product['images'][0]['path']
            image_filename = amazon_product['images'][0]['path'].lstrip('full/')

            product = self.create_product(amazon_product, category, image_filename, image_path)
            retailer_product = self.create_retailer_product(amazon_product, category, image_filename, image_path,
                                                            product, retailer)
            if amazon_product['price']:
                self.create_price(amazon_product, retailer_product)

    @staticmethod
    def detect_women_and_multipack_product(amazon_product):
        amazon_product['is_for_women'] = amazon_product['is_multipack'] = False
        if (re.match(r'women', amazon_product['pet_type'], re.IGNORECASE)
                or re.search(r'for women(?! (?:and|&) men)', amazon_product['title'], re.IGNORECASE)):
            logger.info(f'Detected product for women: {amazon_product["asin"]} {amazon_product["title"]}')
            amazon_product['is_for_women'] = True
        if re.search(r' pack| set|\dx\d', amazon_product['title'], re.IGNORECASE):
            logger.info(f'Detected multipack product: {amazon_product["asin"]} {amazon_product["title"]}')
            amazon_product['is_multipack'] = True
        return amazon_product 

    @staticmethod
    def create_price(amazon_product, retailer_product):
        price = Price(retailer_product=retailer_product,
                      price=amazon_product['price'])
        price.save()

    @staticmethod
    def create_retailer_product(amazon_product, category, image_filename, image_path, product, retailer):
        retailer_product = RetailerProduct(product=product,
                                           retailer=retailer,
                                           category=category.name,
                                           brand=amazon_product['brand'],
                                           manufacturer=amazon_product['manufacturer'],
                                           gender=amazon_product['pet_type'],
                                           country_of_origin=amazon_product['country_of_origin'],
                                           name=amazon_product['title'],
                                           url=amazon_product['url'],
                                           identifier=amazon_product['asin'],
                                           rating_average=amazon_product['rating_average'],
                                           rating_count=amazon_product['rating_count'],
                                           is_for_women=amazon_product['is_for_women'],
                                           is_multipack=amazon_product['is_multipack'])
        with open(image_path, 'rb') as image:
            image_file = File(image)
            retailer_product.std_image.save(image_filename, image_file)
            retailer_product.save()
        return retailer_product

    def create_brand(self, amazon_product):
        if amazon_product['brand']:
            if amazon_product['brand'] == 'Unknown':
                # https://www.amazon.co.uk/No7-Men-Energising-Moisturiser-50ml/dp/B00ZI6TD9U/ref=sr_1_96?dchild=1&keywords=mens+face+moisturiser&qid=1606575172&sr=8-96
                return
            clean_brand = self.BRAND_CLEANUP_DICT.get(amazon_product['brand'], amazon_product['brand'])
        elif amazon_product['manufacturer']:
            clean_brand = self.MANUFACTURER_CLEANUP_DICT.get(amazon_product['manufacturer'], amazon_product['manufacturer'])
        else:
            return

        brand, created = Brand.objects.get_or_create(name=clean_brand)
        return brand

    def create_product(self, amazon_product, category, image_filename, image_path):
        # TODO only create product if we have reviews for it
        if not amazon_product['is_for_women'] and not amazon_product['is_multipack']:
            brand = self.create_brand(amazon_product)
            product = Product(name=amazon_product['title'],
                              category=category,
                              brand=brand)
            with open(image_path, 'rb') as image:
                image_file = File(image)
                product.std_image.save(image_filename, image_file)
                product.save()
        else:
            product = None
        return product

    def handle(self, *args, **options):
        retailer = get_object_or_404(Retailer, name='Amazon UK')
        category = get_object_or_404(Category, name='Face Moisturiser')

        scraped_data_path = '/mnt/5c25bb3f-e899-41a0-98ef-c45d724622a4/Development/amazon-scraper/data/data_deduplicated.json'
        image_directory_path = '/mnt/5c25bb3f-e899-41a0-98ef-c45d724622a4/Development/amazon-scraper/data/img/'

        # df = pd.read_json(scraped_data_path)
        # df['brand_count'] = df['brand'].map(df['brand'].value_counts())
        # df.to_csv('df.csv')
        # import ipdb; ipdb.set_trace()

        amazon_data = self.open_amazon_json_from_file(scraped_data_path)
        self.parse_and_save(amazon_data, category, retailer, image_directory_path)

        # self.stdout.write(self.style.SUCCESS('Successfully updated RetailerProduct model.'))
