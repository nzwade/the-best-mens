from crispy_forms.bootstrap import InlineRadios, StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.forms import (ModelForm, CharField, RadioSelect, ChoiceField, Textarea, EmailField, ModelChoiceField,
                          IntegerField, Form)
from django.forms.widgets import Select

from .models import Review, Profile, CustomUser, Country, UserActivity


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = ('rating', 'review', )
    rating = ChoiceField(widget=RadioSelect, choices=Review.RATING_CHOICES, required=True)
    review = CharField(widget=Textarea(attrs={'rows': 3}), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'
        self.helper.layout = Layout(InlineRadios('rating'), 'review')


class ProfileCreationForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('country', 'gender')
    country = ModelChoiceField(queryset=Country.objects.all(), empty_label=None)  # TODO remove from form and infer from domain?
    gender = ChoiceField(choices=Profile.GENDER_CHOICES)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'
        self.helper.layout = Layout(InlineRadios('gender'), 'country')


class ProfileChangeForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('country', 'gender', 'birth_year', 'skin_type', )
    country = ModelChoiceField(queryset=Country.objects.all(), empty_label=None)
    gender = ChoiceField(choices=Profile.GENDER_CHOICES)
    # When I set this to ChoiceField I couldn't get the blank choice to display :( Using IntegerField instead
    birth_year = IntegerField(required=False)
    skin_type = ChoiceField(label='Skin type', widget=RadioSelect, choices=Profile.SKIN_TYPE_CHOICES, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'
        self.helper.layout = Layout('country', 'birth_year', InlineRadios('gender'), InlineRadios('skin_type'))


class SkinTypeForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['skin_type']
    skin_type = ChoiceField(label='My skin type', widget=RadioSelect, choices=Profile.SKIN_TYPE_CHOICES)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'
        self.helper.layout = Layout(InlineRadios('skin_type'))


class CustomAuthenticationForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ('email', )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'


class CustomUserChangeForm(ModelForm):
    email = EmailField(disabled=True)

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ('email', 'username', 'first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        user_permissions = self.fields.get('user_permissions')
        if user_permissions:
            user_permissions.queryset = user_permissions.queryset.select_related('content_type')

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'


class UserActivityForm(ModelForm):
    class Meta:
        model = UserActivity
        fields = ('status',)

    status = ChoiceField(choices=UserActivity.ACTIVITY_CHOICES,
                         widget=Select(attrs={'onchange': 'form.submit();'}),
                         required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-9'


class FilterForm(Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_tag = False
        self.helper.form_class = 'form-inline'
        # self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout('category', 'brand', 'name',
                                    Submit('submit', 'Submit', style='margin-left: 15px'))
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-1'
        self.helper.form_method = 'GET'
