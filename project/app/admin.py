from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import (Category, Brand, Product, Retailer, Price, Profile, Review, RetailerProduct, Currency, BrandParent,
                     Country, CustomUser, Peta, UserActivity)


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"name_slug": ("name",)}


class BrandAdmin(admin.ModelAdmin):
    prepopulated_fields = {"name_slug": ("name",)}
    list_display = ('name', 'parent', 'peta')


class RetailerProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand', 'manufacturer', 'category', 'retailer', 'identifier', 'is_for_women', 'is_multipack')
    list_filter = ('is_for_women', 'is_multipack')


class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"name_slug": ("name",)}
    list_display = ('name', 'brand', 'category', 'users_using_count')


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'country','birth_year','gender', 'skin_type')


class PetaAdmin(admin.ModelAdmin):
    list_display = ('company_name', 'vegan', 'vegan_notes', 'testing_policy', 'peta_logo', 'brand', 'valid_from', 'valid_to')
    list_filter = ('vegan', 'testing_policy', 'peta_logo', 'valid_from', 'valid_to')
    search_fields = ('company_name',)
    ordering = ('company_name',)


class UserActivityAdmin(admin.ModelAdmin):
    list_display = ('user', 'product', 'status')


admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Retailer)
admin.site.register(Price)
admin.site.register(Review)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(RetailerProduct, RetailerProductAdmin)
admin.site.register(Currency)
admin.site.register(BrandParent)
admin.site.register(Country)
admin.site.register(CustomUser, UserAdmin)
admin.site.register(Peta, PetaAdmin)
admin.site.register(UserActivity, UserActivityAdmin)
